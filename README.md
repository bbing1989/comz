# Co.MZ 企业系统

- 轻量级企业网站管理系统

- 运行环境:PHP5.3+, MySQL5.0

##系统预览

- 系统下载：[http://www.tecmz.com](http://www.tecmz.com)

- 预览地址：[http://co.tecmz.com](http://co.tecmz.com)



##各种设备自适应

- 响应式的网站设计能够对用户产生友好度，并且对于不同的分辨率能够灵活的进行操作应用。 简洁通俗表达就是页面宽度可以自适应屏幕大小，一个网站PC、手机、PAD通吃，页面地址一致。

- 一个字“酷“，可以用PC浏览器拉动窗口大小，网站内容显示依旧在设计之内，用户体验非常不错。 一个字“省”，一个网站PC、手机、PAD通吃，这样就不用花那么多心思去维护多个网站，无论是制作还是数据内容。


##基于HTML5技术

- HTML5对于用户来说，提高了用户体验，加强了视觉感受。HTML5技术在移动端，能够让应用程序回归到网页，并对网页的功能进行扩展，操作更加简单，用户体验更好。 

- HTML5技术跨平台，适配多终端。对于搜索引擎来说，HTML5新增的标签，使搜索引擎更加容易抓取和索引网页，从而驱动网站获得更多的点击流量。


##人性化的后台管理

- 传统的企业网站管理系统是以技术人员的角度出发，设计了很多复杂的功能，并且操作流程上也很复杂，对于最终要操控这个系统的管理员来说并不是很人性化，Co.MZ所做的只是简化不必要的功能，从操作习惯下合理地布局和设计界面，让最普通的用户，即使没有网站管理的经营，也能很容易上手我们的系统。


##伪静态代码
###Apache环境.htaccess文件

	RewriteEngine On
	RewriteBase /
	RewriteRule ^/?$ - [NC,L]
	RewriteRule ^(app/|data/|asserts/|res/|_RUN/|robots\.txt|crossdomain\.xml).*$ - [NC,L]
	RewriteRule ^([a-z0-9]+)\.php - [NC,L]
	RewriteRule ^(.*)$ index.php/$1  [NC,L] 

###Nginx环境
	location / {
    	index index.php;
    	if ( !-e $request_filename ) {
            rewrite ^(.*)$ /index.php?s=$1 last;
            break;
        }
    }

    location ~ \.php$ {
        fastcgi_pass   127.0.0.1:9000;
        fastcgi_index  index.php;
        fastcgi_param  PHP_VALUE  "open_basedir=$document_root:/tmp/";
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~* ^/(app|data|asserts|robots\.txt|crossdomain\.xml)/.*$ {
        if ( -f $request_filename ) {
            expires max;
            break;
        }
    }