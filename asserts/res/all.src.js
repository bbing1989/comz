require(['jquery', 'jquery.extern', 'jquery.lazyload'], function () {
    $(function () {
        $('.box-mod-category .item img').each(function (i, o) {
            $(o).height($(o).width());
        });
        $('.box-product-list .item img').each(function (i, o) {
            $(o).height($(o).width());
        });

        $("img.lazy").lazyload({
            event: "sporty"
        });
        
    });

    $(window).bind("load", function () {
        var timeout = setTimeout(function () {
            $("img.lazy").trigger("sporty")
        }, 5000);
    });
});