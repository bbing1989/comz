require(['jquery', 'bootstrap', 'jquery.extern', 'lhgdialog.lang', 'lhgdialog.base'], function () {
    $(function () {

        $('#frame-menu .m-1').on('click', function () {
            if (!$(this).next().is(':visible')) {
                $('#frame-menu .m-1').next().slideUp();
                $(this).next().slideDown();
            }
            return false;
        });
        $('#frame-menu .m-2').on('click', function () {
            $(this).next().slideDown();
            return false;
        });

        if ($('#btn-module-cms-list').length) {

            var func_module_set = function (enable, obj) {
                var name = $(obj).text();
                var url = '?s=' + TPX.PATH_ROOT + '/helper/module_enable';
                $.get(url, {module: name, enable: enable}, function (data) {
                    $('#btn-module-cms-list').click();
                });
            };
            var func_cms_set = function (enable, obj) {
                var name = $(obj).text();
                var url = '?s=' + TPX.PATH_ROOT + '/helper/cms_enable';
                $.get(url, {cms: name, enable: enable}, function (data) {
                    $('#btn-module-cms-list').click();
                });
            };
            var func_extend_set = function (enable, obj) {
                var name = $(obj).text();
                var url = '?s=' + TPX.PATH_ROOT + '/helper/extend_enable';
                $.get(url, {extend: name, enable: enable}, function (data) {
                    $('#btn-module-cms-list').click();
                });
            };
            var func_framework_set = function (enable, obj) {
                var url = '?s=' + TPX.PATH_ROOT + '/helper/framework_enable';
                $.get(url, {enable: enable}, function (data) {
                    $('#btn-module-cms-list').click();
                });
            };


            var func_module_cms_list = function () {
                var url = $(this).attr('data-url');
                var waiting = $.dialog.tips(lhgdialog_lang.LOADING, 10, 'loading.gif');
                $.post(url, {}, function (data) {
                    waiting.close();
                    if (!data.error) {
                        var box = $('#box-module-list');
                        box.html('');
                        for (var i = 0; i < data.modules.length; i++) {
                            var cls = 'btn-default';
                            if (data.modules[i].enable) {
                                cls = 'btn-primary';
                            }
                            box.append('<a href="#" class="btn btn-xs ' + cls + '">' + data.modules[i].name + '</a>');
                        }
                        box.children('.btn-default').on('click', function () {
                            func_module_set(1, this);
                            return false;
                        });
                        box.children('.btn-primary').on('click', function () {
                            func_module_set(0, this);
                            return false;
                        });

                        box = $('#box-cms-list');
                        box.html('');
                        for (var i = 0; i < data.cmses.length; i++) {
                            var cls = 'btn-default';
                            if (data.cmses[i].enable) {
                                cls = 'btn-primary';
                            }
                            box.append('<a href="#" class="btn btn-xs ' + cls + '">' + data.cmses[i].name + '</a>');
                        }
                        box.children('.btn-default').on('click', function () {
                            func_cms_set(1, this);
                            return false;
                        });
                        box.children('.btn-primary').on('click', function () {
                            func_cms_set(0, this);
                            return false;
                        });

                        box = $('#box-extends-list');
                        box.html('');
                        for (var i = 0; i < data['extends'].length; i++) {
                            var cls = 'btn-default';
                            if (data['extends'][i].enable) {
                                cls = 'btn-primary';
                            }
                            box.append('<a href="#" class="btn btn-xs ' + cls + '">' + data['extends'][i].name + '</a>');
                        }
                        box.children('.btn-default').on('click', function () {
                            func_extend_set(1, this);
                            return false;
                        });
                        box.children('.btn-primary').on('click', function () {
                            func_extend_set(0, this);
                            return false;
                        });


                        box = $('#box-framework');
                        box.html('');
                        var cls = 'btn-default';
                        if (data.framework) {
                            cls = 'btn-primary';
                        }
                        box.append('<a href="#" class="btn btn-xs ' + cls + '">Framework</a>');

                        box.children('.btn-default').on('click', function () {
                            func_framework_set(1, this);
                            return false;
                        });
                        box.children('.btn-primary').on('click', function () {
                            func_framework_set(0, this);
                            return false;
                        });

                    }
                });
                return false;
            };


            var func_init = function () {
                var url = $(this).attr('data-url');
                var waiting = $.dialog.tips(lhgdialog_lang.LOADING, 10, 'loading.gif');
                $.post(url, {}, function (data) {
                    waiting.close();
                    $.defaultFormCallback(data);
                });
                return false;
            };

            $('#btn-module-cms-list').on('click', func_module_cms_list);
            $('#btn-module-cms-list').click();

        }

        $('.command-ajax-request').on('click', function () {
            var url = $(this).attr('data-href');
            var data = $(this).attr('data-data');
            var waiting = $.dialog.tips(lhgdialog_lang.LOADING, 10, 'loading.gif');
            $.post(url, data, function (data) {
                waiting.close();
                $.defaultFormCallback(data);
            });
            return false;
        });

    });


    $(window).on('resize', admin_resize);

    var admin_resize = function () {
        if ($('#frame-content').height() >= $(window).height()) {
            $('#frame-menu').css({
                height: $(window).height() - 55 + 'px'
            });
        } else {
            $('#frame-menu').css({
                height: $(window).height() - 91 + 'px'
            });
        }
        $('#frame-content').css({
            minHeight: $(window).height() - 91 + 'px'
        });

    };

    admin_resize();


});

function admin_grid_delete(url) {
    var ids = [];
    grid.find('input[name=select]:checked').each(function (i, o) {
        var id = parseInt($(o).val());
        if (id) {
            ids.push(id);
        }
    });
    if (ids.length) {
        $.post(url, {ids: ids.join(',')}, function (data) {
            if (data.status && data.info && data.info == 'OK' && data.status == 1) {
                grid.bootgrid('reload');
            } else {
                $.defaultFormCallback(data);
            }
        });
    }
    return false;
}

function admin_grid_delete_one(url, id) {
    $.post(url, {ids: id}, function (data) {
        if (data.status && data.info && data.info == 'OK' && data.status == 1) {
            grid.bootgrid('reload');
        } else {
            $.defaultFormCallback(data);
        }
    });
    return false;
}